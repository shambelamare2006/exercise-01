package main

import (
	"reflect"
	"testing"
)

type City struct {
	Name string
	Sub  string
	Year int
}

type Person struct {
	Name string
	Age  int
}

func TestWalk(t *testing.T) {
	cases := []struct {
		Name  string
		Input interface{}
		Want  []string
	}{
		{
			Name: "struct with one string field",
			Input: struct {
				Name string
			}{"chris"},
			Want: []string{"chris"},
		},
		{
			Name: "struct with two string field",
			Input: struct {
				Name string
				City string
			}{"chris", "London"},
			Want: []string{"chris", "London"},
		},
		{
			Name: "struct with Nested struct",
			Input: struct {
				Name string
				City struct {
					County    string
					SubCounty string
				}
			}{"chris", struct {
				County    string
				SubCounty string
			}{"Addis Ababa", "Bole"}},
			Want: []string{"chris", "Addis Ababa", "Bole"},
		},
		{
			"struct with flat pointer",
			&Person{"shambel", 12},
			[]string{"shambel"},
		},
		{
			Name: "struct with Nested pointer",
			Input: struct {
				Name string
				City *City
			}{"chris", &City{Name: "Addis Ababa", Sub: "Bole", Year: 1221}},
			Want: []string{"chris", "Addis Ababa", "Bole"},
		},
	}

	for _, test := range cases {
		t.Run(test.Name, func(t *testing.T) {
			var got []string

			Walk(test.Input, func(input string) {
				got = append(got, input)
			})
			if !reflect.DeepEqual(got, test.Want) {
				t.Errorf("want '%v' but got '%v' ", test.Want, got)
			}
		})
	}

}
